from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Kos)
admin.site.register(Pajak)
admin.site.register(Kecamatan)
admin.site.register(Kelurahan)
