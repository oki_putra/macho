from django.apps import AppConfig


class AdminkosConfig(AppConfig):
    name = 'adminkos'
