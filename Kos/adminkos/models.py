from django.db import models

# Create your models here.
class Kos(models.Model):
    id_kelurahan = models.ForeignKey('adminkos.Kelurahan', on_delete=models.CASCADE)
    nama = models.CharField(max_length=200)
    pemilik = models.CharField(max_length=200)
    alamat = models.TextField(max_length=200)
    penghasilan = models.IntegerField()
    kapasitas = models.IntegerField()
    hargaSewa = models.IntegerField()
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=20)
    isi  = models.IntegerField()

    def __str__(self):
        return self.nama

class Pajak(models.Model):
    id_kos = models.ForeignKey('adminkos.Kos', on_delete=models.CASCADE)
    penghasilan = models.IntegerField()
    target = models.IntegerField()
    tanggal = models.DateTimeField()

class Kecamatan(models.Model):
    nama = models.CharField(max_length=200)

    def __str__(self):
        return self.nama

class Kelurahan(models.Model):
    id_kecamatan = models.ForeignKey('adminkos.Kecamatan', on_delete=models.CASCADE)
    nama = models.CharField(max_length=200)
    lurah = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

    def __str__(self):
        return self.nama