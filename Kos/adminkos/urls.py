from django.urls import path, include
from .views import *


urlpatterns = [
    path('datakos', DataKos.index, name='list_kost'),
    path('datakos/kelurahan/<int:id>', DataKos.kelurahan, name='kos_kelurahan'),
    path('datakos/kecamatan/<int:id>', DataKos.kecamatan, name='kos_kecamatan'),
]