from django.shortcuts import render
from . models import *

# Create your views here.
class DataKos:
    def index(request):
        list_kos = Kos.objects.all()
        return render(request, "kos.html", {"list_kos": list_kos})
    
    def kelurahan(request, id = None ):
        list_kos = Kos.objects.filter(id_kelurahan=id)
        return render(request, "koskelurahan.html", {"list_kos":list_kos})

    def kecamatan(request, id = None ):
        list_kecamatan = Kecamatan.objects.filter(pk=id)
        list_kelurahan = Kelurahan.objects.filter(id_kecamatan=list_kecamatan)
        list_kos = Kos.objects.filter(id_kelurahan=list_kelurahan)
        return render(request, "koskelurahan.html", {"list_kos":list_kos})